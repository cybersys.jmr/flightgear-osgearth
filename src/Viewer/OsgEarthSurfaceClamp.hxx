//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifndef __OsgEarthSurfaceClamp_h__
#define __OsgEarthSurfaceClamp_h__

#ifdef ENABLE_OSGEARTH

#include <osg/Referenced>
#include <osg/ref_ptr>
#include <osg/observer_ptr>
#include <osg/Node>
#include <osg/NodeVisitor>
#include <osgEarth/Terrain>
#include <simgear/scene/model/SGOffsetTransform.hxx>
#include <simgear/scene/model/ModelRegistry.hxx>
#include <simgear/threads/SGGuard.hxx>
#include "OsgEarthObjectListener.hxx"
#include "OsgEarthClampLightsVisitor.hxx"


class OsgEarthSurfaceClamp : public osg::Referenced
{
public:
    typedef std::map<std::string, osg::ref_ptr<OsgEarthClampLightsVisitor> > 
        LightsClampMapType;

public:
    // constructor
    OsgEarthSurfaceClamp()
    :   osg::Referenced(),
        m_Objects(new OsgEarthObjectListener),
        m_ClampedAirportMap(),
        m_AirportPosMap()
    {}

protected:
    // destructor
    ~OsgEarthSurfaceClamp() {}

public:
    // continually clamp current object set
    void updateSurfaceClamping(const float rangeNm);

    // reset surface light clamping
    void ResetClamping(); 

protected:

    // ensure lights are surface clamped
    void MonitorAirportLighting(const std::vector<std::string>& airportsInRange);

protected:


    // static object listener
    osg::ref_ptr<OsgEarthObjectListener> m_Objects;

    // map of nearby airport lights
    LightsClampMapType m_ClampedAirportMap;

    // associative map for nearby airports [airport name, airport position]
    std::map<std::string, SGGeod> m_AirportPosMap;


};


#endif

#endif
