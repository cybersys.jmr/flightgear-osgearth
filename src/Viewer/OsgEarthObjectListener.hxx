//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifndef __OsgEarthObjectListener_h__
#define __OsgEarthObjectListener_h__

#ifdef ENABLE_OSGEARTH

#include <osg/Referenced>
#include <osg/ref_ptr>
#include <osg/observer_ptr>
#include <osg/Node>
#include <osg/NodeVisitor>
#include <osgEarth/Terrain>
#include <simgear/scene/model/SGOffsetTransform.hxx>
#include <simgear/scene/model/ModelRegistry.hxx>
#include <simgear/threads/SGGuard.hxx>

class SurfaceObject : public osg::Referenced {
public:
    enum ObjectClampState
    {
        WaitingToBeClamped = 0,
        DoNotClamp,
        IsClamped
    };

public:
    SurfaceObject(std::string& name, ObjectClampState state) 
        :   osg::Referenced(),
        m_Name(name),
        m_State(state)
    {}

public:
    std::string m_Name;
    ObjectClampState m_State;
};


class OsgEarthObjectListener : public simgear::StaticObjectListener
{
public:

    typedef std::map<
        osg::observer_ptr<osg::MatrixTransform>, 
        osg::ref_ptr<SurfaceObject> > ObjectClampMapType;


public:
    // constructor
    OsgEarthObjectListener()
    :   simgear::StaticObjectListener(),
        m_AddObjectLock()
    {
        simgear::ModelRegistry::instance()->addStaticObjectListener(this);
    }

protected:
    // destructor
    ~OsgEarthObjectListener() {}

public:
    virtual void OnAddObject(std::string& name, osg::MatrixTransform* mt);

    // continually clamp current object set
    void updateSurfaceClamping(const float rangeNm);

    // reset surface light clamping
    void ResetClamping(); 

    void MonitorAirportLighting(const std::vector<std::string>& airportsInRange);

    void MonitorObjectClamping();


protected:

    // static object clamp state map
    ObjectClampMapType m_ObjectClampMap;
    
    // protect from multiple database pager threads
    SGMutex m_AddObjectLock;

};


#endif

#endif
