//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifdef ENABLE_OSGEARTH

#include <osg/TextureRectangle>
#include <osg/Texture2D>
#include <osg/ComputeBoundsVisitor>
#include <osgDB/WriteFile>
#include <osgEarth/VirtualProgram>
#include <osgEarth/ElevationQuery>
#include <Airports/airport.hxx>
#include <Airports/runwaybase.hxx>
#include <Airports/runways.hxx>
#include <osgViewer/Viewer>
#include "renderer.hxx"
#include <osgText/Text>
#include <Main/globals.hxx>
#include <Main/fg_props.hxx>
#include <boost/format.hpp>
#include <osgDB/ReadFile>
#include <osg/MatrixTransform>
#include <GL/glu.h>
#include <osgDB/FileNameUtils>
#include <Scenery/scenery.hxx>
#include <boost/algorithm/string.hpp>    
#include <simgear/scene/util/OsgMath.hxx>

#ifdef ENABLE_OSGEARTH
#   include <simgear/scene/util/OsgEarthHeightCache.hxx>
#   include <osgEarthAnnotation/ModelNode>
#endif

#include "OsgEarthObjectListener.hxx"

void OsgEarthObjectListener::OnAddObject(std::string& name, osg::MatrixTransform* mt)
{
    // protect state from multiple database pager threads 
    SGGuard<SGMutex> g(m_AddObjectLock);

    // add / update entry
    m_ObjectClampMap[mt] = new SurfaceObject(name, SurfaceObject::WaitingToBeClamped);
}

void OsgEarthObjectListener::MonitorObjectClamping()
{
    // --- check for object expiration ---
    {
        std::vector<ObjectClampMapType::iterator> invalidateList;

        ObjectClampMapType::iterator it;
        for (it = m_ObjectClampMap.begin(); it != m_ObjectClampMap.end(); ++it) {
            osg::MatrixTransform* xform = it->first.get();

            if (xform == NULL) {
                invalidateList.push_back(it);
                SG_LOG(SG_TERRAIN, SG_INFO, "[OsgEarthObjectListener] "
                    "invalidating clamped model: " << it->second->m_Name);
            }
        }

        for (unsigned int i = 0; i < invalidateList.size(); ++i) {
            m_ObjectClampMap.erase(invalidateList[i]);
        }
    }

    // --- Clamp models to surface ---


    if (simgear::OsgEarthHeightCache::Instance()->IsWithinTolerance() && 
        !simgear::OsgEarthHeightCache::Instance()->GetIsUseCoarseResolution()) 
    {

        ObjectClampMapType::iterator it;
        for (it = m_ObjectClampMap.begin(); it != m_ObjectClampMap.end(); ++it) {
            // reference the object to ensure it exists while we update it
            osg::ref_ptr<osg::MatrixTransform> xform = it->first.get();
            SurfaceObject* obj = it->second;
            if ((xform != NULL) && (obj != NULL)) {
        
                if (obj->m_State == SurfaceObject::WaitingToBeClamped) {
                    osg::Matrixd mat = xform->getMatrix();

                    // distance-squared, meters^2, that object clamping will occur
                    const float minD2 = osg::square(1000.0f);
                    // note: consider scaling this distance threshold by FOV
                    osg::Vec3d ownshipPos = toOsg(SGVec3d::fromGeod(
                        globals->get_aircraft_position()));
                    double dist2 = (ownshipPos - mat.getTrans()).length2();

                    if (dist2 <= minD2) {
                        SGGeod geodPos = SGGeod::fromCart(toSG(mat.getTrans()));


                        double heightMsl = 0.0;
                        osg::Vec3d normal;
                        bool isHit = simgear::OsgEarthHeightCache::Instance()->
                            FindTerrainHeightInScene(
                                globals->get_aircraft_position(), 
                                heightMsl, 
                                normal);
                        if (isHit) {

                            const float tooFarMeters = 20.0;
                            if (fabs(geodPos.getElevationM() - heightMsl) > tooFarMeters) {
                                // excessive change, so don't clamp
                                obj->m_State = SurfaceObject::DoNotClamp;
                                continue;
                            }

                            geodPos.setElevationM(heightMsl);

                            mat.setTrans(toOsg(SGVec3d::fromGeod(geodPos)));
                            if (xform != NULL) {
                                xform->setMatrix(mat);
                            }

                            obj->m_State = SurfaceObject::IsClamped;

                            SG_LOG(SG_TERRAIN, SG_INFO, 
                                boost::format(
                                    "[OsgEarthObjectListener] clamped a model "
                                    "[%s]; Elev [%.2f],  count: [%d]") 
                                    % obj->m_Name.c_str()
                                    % geodPos.getElevationM()
                                    % m_ObjectClampMap.size());
                        }
                    }
                }
            }
        }
    }
}

// reset surface light clamping
void OsgEarthObjectListener::ResetClamping()
{
    m_ObjectClampMap.clear();
}


#endif